JasDB is an open Document based database that can be used to store
unstructured data. The database can be used standalone and accessed
through a java client library or the REST interface.

Also the database can be used in process of your Java application,
ideal for embedded projects where a small lightweight database is needed.

JasDB has the folowing features:
* Lightweight memory and cpu profile
* High throughput on a single machine
* Full query capabilities
* BTree index structure
* REST webservice
* Java client API (both for remote and local embedded mode)

For more information have a look at http://www.oberasoftware.com or 
the wiki https://bitbucket.org/oberasoftware/jasdb_open/wiki
